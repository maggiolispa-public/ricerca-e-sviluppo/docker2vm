# Convertitore di immagini Docker in dischi virtuali per VM

Una volta creata un **Dockerfile** con tutto quello che serve per avere un template di VM, si può utilizzare il comando `create_vdisk` per creare il disco virtuale.

I formati di disco supportati sono: `qcow2` `qed` `raw` `vdi` `vhd` `vmdk`.

Nel caso di `vmdk` si tratta del formato **Workstation**. Quindi per poterlo utilizzare su un sistema **ESXi** sarà necessario convertirlo direttamente sul server aprendo una sessione **ssh** e utilizzando il comando

```bash
    vmkfstools -i disco_vmworkstation.vmdk disco_esxi.vmdk
```

A questo punto il disco convertito sarà riconosciuto correttamente da **ESXi** e potrà essere utilizzato.

