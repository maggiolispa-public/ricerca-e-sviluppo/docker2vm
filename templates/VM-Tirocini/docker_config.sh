#!/bin/bash

service docker stop

cp daemon.json /etc/docker/daemon.json
    
rsync -aP /var/lib/docker /mnt/dati

rm -rf /var/lib/docker.old

service docker start


