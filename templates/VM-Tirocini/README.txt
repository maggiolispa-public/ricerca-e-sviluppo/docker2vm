==============================
 VM Ubuntu 23.04.3-LTS-Server
==============================

Questa immagine è studiata per i laboratori dei tirocini di A.I.


Operazioni conclusive all'installazione
---------------------------------------

L'immagine è parzialmente configurata. Rimangono alcuni passi da eseguire.

1. Aggiungere un file di swap con i seguenti comandi shell:

      fallocate -l 16G /swapfile
      dd if=/dev/zero of=/swapfile bs=1024 count=16777216
      chmod 600 /swapfile
      mkswap /swapfile
      echo "/swapfile swap swap defaults 0 0" >> /etc/fstab
      swapon -a
   
2. Montare in `/mnt/dati` un disco capiente (1 TB) che verrà utilizzato come 
   repository dei container Jupyter. Utilizzare il seguente comando shell:

      echo "/dev/sdb1 /mnt/dati ext4 rw,relatime 0 2" >> /etc/fstab

3. Completare la configurazione di docker (impostazione del runtime NVidia, 
   indirizzi IP utilizzati, directory delle immagini e volumi) avviando il 
   comando `docker_config.sh` presente nella directory `/root/tools`

4. Cambiare il nome dell'host editando i file `/etc/hostname` e `/etc/hosts`

5. Impostazione dell'IP statico editando il file `/etc/netplan/*.yaml`

     network:
        ethernets:
           <INTERFACE_LOGICAL_NAME>:
              dhcp4: no
              addresses: [172.17.84.<ADDRESS>/24]
              gateway4: 172.17.84.1
              nameservers:
                  addresses: [8.8.8.8, 8.8.4.4]
        version: 2

   dove:

   - <INTERFACE_LOGICAL_NAME> lo si ottiene eseguendo il comando shell 
       `lshw -C network`
   - <ADDRESS> è l'ultima parte dell'indirizzo che si vuole per la VM 

   Infine eseguire il comando shell `netplan apply`

6. Configurare `rclone` per collegarsi ad un disco MS-Onedrive


Utenti
------

L'utente principale è `root`.
E' presente anche un utente `jovyan` con `UID=1000` e `GUID=1000` che ha come
unico scopo quello di mappare l'utente di lavoro dei vari container Jupyter.


Package installati
------------------

- git
- sshfs
- ssh-client
- ssh-server
- wget
- docker
- nvidia-docker
- sen
- ctop
- rsync
- rclone
- byobu
- btop
- nvtop
- nvidia-driver
- nvidia-cuda
- nano
- python
- pip
- mg_jupyter
- open-vm-tools
- fdisk